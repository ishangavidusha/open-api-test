import json
import os

ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

def _readJson(file) -> dict:
    with open(file, 'r') as f:
        return json.load(f)
    
def _writeJson(file, data) -> bool:
    with open(file, 'w') as f:
        json.dump(data, f, indent=4)
        return True
        
def get_db_posts():
    return _readJson(os.path.join(ROOT_PATH, 'posts.json'))

def write_to_posts(posts):
    return _writeJson(os.path.join(ROOT_PATH, 'posts.json'), posts)

def get_db_users():
    return _readJson(os.path.join(ROOT_PATH, 'users.json'))

def write_to_users(users):
    return _writeJson(os.path.join(ROOT_PATH, 'users.json'), users)