from datetime import datetime, timezone
from fastapi import Depends, FastAPI, HTTPException
from models import EmediaApi, Ping
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import APIKeyHeader

auth = APIKeyHeader(name="Authorization")

from routes.post import router as post_router
from routes.user import router as user_router

app: FastAPI = FastAPI(
    title="Emedia Open API Test",
    version="0.1.0",
    redoc_url=None,
    openapi_url="/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"]
)

@app.get("/", response_model=EmediaApi, tags=["Root"])
def root():
    return EmediaApi(
        name=app.title,
        version=app.version
    )

@app.get("/ping", response_model=Ping, tags=["Ping"])
def ping():
    return Ping(message="pong", time=datetime.now(timezone.utc).isoformat())

def validate_api_key(api_key: str = Depends(auth)):
    if api_key != "v2xczi6vcooxkdtc4kvq3eyntjvpese9":
        raise HTTPException(status_code=400, detail="Invalid API key")
    
app.include_router(post_router, prefix="/posts", tags=["Posts"])
app.include_router(user_router, prefix="/users", tags=["Users"], dependencies=[Depends(validate_api_key)])