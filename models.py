from __future__ import annotations
from pydantic import BaseModel

class EmediaApi(BaseModel):
    name: str
    version: str

class Ping(BaseModel):
    message: str
    time: str
    
class Posts(BaseModel):
    userId: int
    id: int
    title: str
    body: str

class Geo(BaseModel):
    lat: str
    lng: str


class Address(BaseModel):
    street: str
    suite: str
    city: str
    zipcode: str
    geo: Geo


class Company(BaseModel):
    name: str
    catchPhrase: str
    bs: str


class User(BaseModel):
    id: int
    name: str
    username: str
    email: str
    address: Address
    phone: str
    website: str
    company: Company