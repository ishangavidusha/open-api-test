from typing import List
from fastapi import APIRouter, Depends, HTTPException
from db.db import get_db_posts, write_to_posts
from models import Posts


router = APIRouter()

@router.get("", response_model=List[Posts])
def get_posts():
    db_posts = get_db_posts()
    return [Posts(**post) for post in db_posts]

@router.get("/{id}", response_model=Posts)
def get_post(id: int, db_posts: List[Posts] = Depends(get_db_posts)):
    post = next((p for p in db_posts if p.id == id), None)
    if not post:
        raise HTTPException(status_code=404, detail="Posts not found")
    return Posts(**post)

@router.post("", response_model=Posts)
def create_post(post: Posts):
    db_posts = get_db_posts()
    db_posts.append(post.dict())
    write_to_posts(db_posts)
    return Posts(**post)

@router.put("/{id}", response_model=Posts)
def update_post(id: int, post: Posts, db_posts: List[Posts] = Depends(get_db_posts)):
    post_in_db = next((p for p in db_posts if p.id == id), None)
    if not post_in_db:
        raise HTTPException(status_code=404, detail="Posts not found")
    post_in_db.update(post.dict())
    write_to_posts(db_posts)
    return Posts(**post_in_db)

@router.delete("/{id}", response_model=Posts)
def delete_post(id: int, db_posts: List[Posts] = Depends(get_db_posts)):
    post_in_db = next((p for p in db_posts if p.id == id), None)
    if not post_in_db:
        raise HTTPException(status_code=404, detail="Posts not found")
    db_posts.remove(post_in_db)
    write_to_posts(db_posts)
    return Posts(**post_in_db)