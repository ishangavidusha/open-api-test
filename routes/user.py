from typing import List
from fastapi import APIRouter, Depends, HTTPException
from db.db import get_db_users, write_to_users
from models import User

router = APIRouter()

@router.get("", response_model=List[User])
def get_users():
    db_users = get_db_users()
    return [User(**user) for user in db_users]

@router.get("/{id}", response_model=User)
def get_user(id: int, db_users: List[User] = Depends(get_db_users)):
    user = next((u for u in db_users if u.id == id), None)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return User(**user)

@router.post("", response_model=User)
def create_user(user: User):
    db_users = get_db_users()
    db_users.append(user.dict())
    write_to_users(db_users)
    return User(**user)

@router.put("/{id}", response_model=User)
def update_user(id: int, user: User, db_users: List[User] = Depends(get_db_users)):
    user_in_db = next((u for u in db_users if u.id == id), None)
    if not user_in_db:
        raise HTTPException(status_code=404, detail="User not found")
    user_in_db.update(user.dict())
    write_to_users(db_users)
    return User(**user_in_db)

@router.delete("/{id}", response_model=User)
def delete_user(id: int, db_users: List[User] = Depends(get_db_users)):
    user_in_db = next((u for u in db_users if u.id == id), None)
    if not user_in_db:
        raise HTTPException(status_code=404, detail="User not found")
    db_users.remove(user_in_db)
    write_to_users(db_users)
    return User(**user_in_db)